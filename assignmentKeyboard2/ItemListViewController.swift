//
//  ViewController.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 08/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import UIKit
import Foundation


class ItemListViewController: UIViewController {

    var items: [Item] = []
    var filteredItems: [Item] = []
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        refreshItemList()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowNewItemViewController" {
            let destVC: NewItemViewController = segue.destination as! NewItemViewController
                destVC.delegate = self
            }
        
    }
    
    @IBAction func filterTableView(_ sender: Any) {
        //clear the filteredItems array for next use
        filteredItems.removeAll()
        
        var noResult : Bool = true
        
        if let searchField : String = searchTextField.text {
            for item in items {
                if (item.itemName?.lowercased())?.range(of: searchField) != nil {
                    filteredItems.append(item)
                    noResult = false
                }
            }
            // reset the tableview with the filteredItems array
            self.tableView.reloadData()
           
        }
        
        if noResult == true {
            let alert : UIAlertController = UIAlertController(title: "NO RESULT" , message: "This is an alert" , preferredStyle : UIAlertControllerStyle.alert)
            present(alert , animated: true , completion: nil)
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func showEditingMode(_ sender: Any) {
        
    
        if self.tableView.isEditing {
            self.tableView.setEditing(false, animated: true)
        } else {
            self.tableView.setEditing(true, animated: true)
        }
        
        
    }
    
    func refreshItemList(){
        //code
        var itemList: [Item] = []
        ItemLoader.sharedLoader.retrieveDataFromAPI { (itemList2: [Item],success: Bool, error: Error?) in
            //code
            self.items = itemList2
            self.tableView.reloadData()
        }
    }
}
extension ItemListViewController : NewItemDelegate{
    
    func viewController(vc: NewItemViewController, didAddItem newItem: Item!){
        //received the delegation of newItem from the NewItemViewController
        self.items.append(newItem)
        //basicly add the item to the items array
        
        ItemLoader.sharedLoader.postDataToAPI(newItem: newItem) { (success: Bool, error: Error?) in
        }
        //retrieve object id in API to sync it with the application
        self.refreshItemList()
        self.dismiss(animated: true, completion: nil)
        //the NewItemViewController are dismissed
        self.tableView.reloadData()
        //after that the table view were reloaded to accomodate the newly added item
    }
}
// basicly the UITableViewSources need these 2 function to work to display the data into table view cell
extension ItemListViewController : UITableViewDataSource{
    //function 1
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredItems.isEmpty{
           return items.count
        } else {
            return filteredItems.count
        }
        //returning the number of section to be handled and for this case we want it to be the same with the number of item in items array
    }
    //function 2
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")
        
        //assign the newly created cell object of UITableVIewCell to a UITableViewCell with default style ...
        if filteredItems.isEmpty{
        cell.textLabel?.text = items[indexPath.row].itemName
        cell.detailTextLabel?.text = items[indexPath.row].itemDesc

        } else {
            cell.textLabel?.text = filteredItems[indexPath.row].itemName
            cell.detailTextLabel?.text = filteredItems[indexPath.row].itemDesc
        }
        return cell
        //return the cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        //code
        let cell  : UITableViewCellEditingStyle = UITableViewCellEditingStyle.delete
        
        return cell
    }
    
    func  tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        //code deletion
        let objectAPIID: String = items[indexPath.row].objectAPIID!
        ItemLoader.sharedLoader.deleteDataFromAPI(objectID: objectAPIID) { (success: Bool, error: Error?) in
            //code
        }
        self.items.remove(at: indexPath.row)
        self.tableView.reloadData()
    }
}

//to make the cell to be able to handle user tap make this extension

extension ItemListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //transfer the object to the itemDetailsViewController
        let itemSelected: Item = self.items[indexPath.row]
        
        let detailsVC: ItemDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailViewController") as! ItemDetailViewController
        
        //instantiate the details view controller (the controller you want to display) and show it
        
        detailsVC.itemSelected = itemSelected
        self.showDetailViewController(detailsVC, sender: self)
        
        //no navigation controller in the storyboard thats why it doesnt work and the navigation controller are optional "?" sign thus making the code runnable
       
    }
}


