//
//  itemLoader.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 11/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import Foundation
//@testable import assignmentKeyboard2

class ItemLoader {
    
    static let sharedLoader: ItemLoader = ItemLoader()
    
    var session: URLSession
    let targetURL: URL
    let config: URLSessionConfiguration
    
    init() {
        config = URLSessionConfiguration.default
        targetURL = URL(string: "https://api.backendless.com/6CC9BE36-075B-0B49-FF82-4BB76CDE1100/2B718DF6-52A9-5721-FFD7-45E4E9315C00/data/Item")!
        self.session = URLSession(configuration: config)
    }
}
extension ItemLoader {
    func retrieveDataFromAPI(completionBlock: @escaping ((_ itemList: [Item],_ success: Bool, _ error: Error?) -> Void)) {
        
        var itemList: [Item] = []
        let loadTask: URLSessionDataTask = self.session.dataTask(with: targetURL) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if error != nil {
                DispatchQueue.main.async {
                    completionBlock([], false, error!)
                }
            }
            
            // completion block starts here
            // server has already responded at this point
            let httpCode : Int = (response as! HTTPURLResponse).statusCode
            
            //check connection
            if httpCode != 200 {
                print("Status Not OK with httpCode : \(httpCode)")
                // call completion with error status
            }
            let jsonData : [[String : AnyObject]] = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.init(rawValue: 0)) as! [[String : AnyObject]]
            
            for itemJson in jsonData {
                let itemName: String = itemJson["itemName"] as! String
                let itemDesc: String = itemJson["itemDesc"] as! String
                let objectAPIID: String = itemJson["objectId"] as! String
                let item: Item = Item(objectAPIID: objectAPIID, itemName: itemName, itemDesc: itemDesc, itemQty: 0)
                itemList.append(item)
                //data received
            }
            
            DispatchQueue.main.async {
                completionBlock(itemList,true,nil)
            }
                // completion block ends here
            
        }
    loadTask.resume()
    }//post data code
    func postDataToAPI(newItem: Item, completionBlock: @escaping ((_ success: Bool, _ error: Error?) -> Void)) {
        var urlRequest: URLRequest = URLRequest(url: targetURL)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = newItem.getJSONData()
        let postTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            let httpCode: Int = (response as! HTTPURLResponse).statusCode
            if httpCode == 200 || httpCode == 201 {
                completionBlock(true, nil)
            } else {
                completionBlock(false, nil)
              }
        }
        postTask.resume()
    }//delete data code
    func deleteDataFromAPI(objectID: String, completionBlock: @escaping ((_ success: Bool, _ error: Error?) -> Void)) {
        let newTargetURL: URL = targetURL.appendingPathComponent(objectID)
        var urlRequest: URLRequest = URLRequest(url: newTargetURL)
        urlRequest.httpMethod = "DELETE"
        let deleteDataTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            //code
            print ("CHANGED item url: \(newTargetURL.absoluteString)")
            let httpCode: Int = (response as! HTTPURLResponse).statusCode
            if httpCode == 200 {
                completionBlock(true,nil)
            } else {
                completionBlock(false,nil)
             }
        }
        deleteDataTask.resume()
    }
}
