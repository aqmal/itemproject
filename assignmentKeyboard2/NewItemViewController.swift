//
//  NewItemViewController.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 08/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import UIKit

protocol NewItemDelegate {
    func viewController(vc: NewItemViewController, didAddItem newItem: Item!)
}

class NewItemViewController: UIViewController {
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var confirmButton: UIButton!
   
  
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var itemDescTextField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        itemNameTextField.becomeFirstResponder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    var newItem: Item?
    var delegate: NewItemDelegate?
    
    @IBAction func confirmButton(_ sender: Any) {
       
        if let itemName = itemNameTextField.text {
            if let itemDesc = itemDescTextField.text {
                newItem = Item(objectAPIID: "", itemName: itemName , itemDesc: itemDesc , itemQty: 0)
                //create newItem object from the text field (data inputted from user)
            }
        }

        self.delegate?.viewController(vc: self, didAddItem: self.newItem)
        //delegate the newItem object
    }
    
    @IBAction func dismissCurrentVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

   

}
