//
//  File.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 08/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import Foundation

class Item {
    var itemName: String?
    var itemDesc: String?
    
    weak var delegate: NewItemDelegate?
    
    init(itemName: String, itemDesc: String) {
        self.itemName = itemName
        self.itemDesc = itemDesc
    }
    func requestData(itemName: String, itemDesc: String) {
        self.itemName = itemName
        self.itemDesc = itemDesc
        delegate?.didReceiveDataUpdate(didAddItem: Item)
        
    }
}
