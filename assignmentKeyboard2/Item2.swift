//
//  File.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 08/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import Foundation


class Item{
    var objectAPIID: String?
    var itemName: String?
    var itemDesc: String?
    var itemQty: Int?

    
    
    init(objectAPIID: String,itemName: String , itemDesc: String , itemQty: Int) {
        self.objectAPIID = objectAPIID
        self.itemName = itemName
        self.itemDesc = itemDesc
        self.itemQty = itemQty
    }
    
    func getJSONData() -> Data {
        let dictA: Dictionary = [
            "ItemName":self.itemName,
            "ItemDesc":self.itemDesc
        ]
        
        let convertedData: Data = try! JSONSerialization.data(withJSONObject: dictA, options: JSONSerialization.WritingOptions.init(rawValue: 0)) 
        
        return convertedData
    }

}
