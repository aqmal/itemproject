//
//  ItemDetailViewController.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 08/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import UIKit

class ItemDetailViewController: UIViewController {

    var itemSelected : Item?
    
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDescLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.itemNameLabel.text = itemSelected?.itemName
        self.itemDescLabel.text = itemSelected?.itemDesc
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
 

}
