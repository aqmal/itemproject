//
//  ItemAPITest.swift
//  assignmentKeyboard2
//
//  Created by Aqmal on 10/05/2017.
//  Copyright © 2017 Aqmal. All rights reserved.
//

import XCTest
@testable import assignmentKeyboard2

class ItemAPITest: XCTestCase {
    
    var session: URLSession!
    
    override func setUp() {
        super.setUp()
        let config: URLSessionConfiguration = URLSessionConfiguration.default
        
        //config.httpAdditionalHeaders.value
        
        
        
        
        self.session = URLSession(configuration: config)
        
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadHomepage() {
        
        let expectation = self.expectation(description: "Load Task")
        
        let url: URL = URL(string: "https://api.backendless.com/6CC9BE36-075B-0B49-FF82-4BB76CDE1100/2B718DF6-52A9-5721-FFD7-45E4E9315C00/data/Item")!

        
        let loadTask: URLSessionDataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
           // let htmlStringData: String = String(data: data!,encoding: String.Encoding.utf8)!
            
            
            NSLog("Task Completed")
            expectation.fulfill()
            //print(htmlString)
        }

        loadTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    
    func testLoadingJSONDatafromAPI() {
        
        let expectation = self.expectation(description: "Load Task")
        let url: URL = URL(string: "https://api.backendless.com/6CC9BE36-075B-0B49-FF82-4BB76CDE1100/2B718DF6-52A9-5721-FFD7-45E4E9315C00/data/Item")!
        
        
        
        let loadTask: URLSessionDataTask = self.session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            
            let json : [[String : AnyObject]] = try! JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.init(rawValue: 0)) as! [[String : AnyObject]]

            
            
            for itemJson in json {
                let itemName: String = itemJson["itemName"] as! String
                let itemDesc: String = itemJson["itemDesc"] as! String
            
                print("Item Name : \(itemName)  Item Desc : \(itemDesc)")
                let newItem: Item = Item(itemName: itemName,itemDesc: itemDesc, itemQty: 0)
            }
            
            NSLog("Task Completed")
            expectation.fulfill()
        
        }
        
        loadTask.resume()
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    
    
    func testCreatingItemOnServer() {
        
        let expectation = self.expectation(description: "Load Task")
        let url: URL = URL(string: "https://api.backendless.com/6CC9BE36-075B-0B49-FF82-4BB76CDE1100/2B718DF6-52A9-5721-FFD7-45E4E9315C00/data/Item")!
        
        var urlRequest: URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let jsonDict: [String : String] = [
            "itemName":"Lamb",
            "itemDesc":"Well done cooked lamb chop"
        ]
        
        let jsonData: NSData = try! JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.init(rawValue: 0)) as NSData
        urlRequest.httpBody = jsonData as Data
        
        
        let postTask: URLSessionDataTask = self.session.dataTask(with: urlRequest) { (data: Data?, response: URLResponse?, error: Error?) in
            //code
            let httpCode: Int = (response as! HTTPURLResponse).statusCode
            XCTAssert(httpCode == 200, "Server should have return code 200 OK")
            expectation.fulfill()
        }
        
        
        postTask.resume()
        
        self.waitForExpectations(timeout: 10, handler: nil)
     
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
